/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  StyleSheet,
  Linking, // For phone calls
  Platform, // For phone calls
  PermissionsAndroid, // For android
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
} from 'react-native/Libraries/NewAppScreen';

import {WebView} from 'react-native-webview';

// Para probar el emulador de Android lanzando 'npm run android'
// tendremos que abrir Android Studio y lanzar en Tools > AVD Manager
// alguno de los emuladores que tengamos. Luego podremos lanzar el comando

// ------- Geolocation --------
// For android -------
// Necesitamos solicitar el permiso
// Hay 3 puntos donde tocamos: 1. Añadir el import arriba
// 2. aqui añadir para que se muestre el popup de autorización
// 3. Añadir en el AndroidManifes.xml:
// <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
PermissionsAndroid.request(
  PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
  {
    title: 'Permiso pa acceder',
    message: 'Nos da permiso para entra pisha?',
    buttonPositive: 'Dale on pare',
  },
);
// ---------------

// For iOS
// Añadir descripcion a Info.plist NSLocationWhenInUseUsageDescription
// Apple permisos: https://developer.apple.com/library/archive/documentation/General/Reference/InfoPlistKeyReference/Articles/CocoaKeys.html
// ------- END Geolocation --------

// ------- Phone Call --------
// 1. Importar import {Linking, Platform,PermissionsAndroid} from 'react-native';
// 2. Capturar el paso de mensaje, en este caso creado por nosotros, de tipo 'phone' mediante onMessage en WebView
// 3. Añadir en el AndroidManifes.xml:
// <uses-permission android:name="android.permission.CALL_PHONE" />
function nativeCallPhone(number) {
  let phoneNumber = '';

  if (Platform.OS === 'android') {
    phoneNumber = `tel:${number.replace(/ /g, '')}`;
  } else {
    phoneNumber = `telprompt:${number.replace(/ /g, '')}`;
  }

  Linking.openURL(phoneNumber);
}
// Activaremos el popup por si acaso. Aunque no tendría por qué estar
PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CALL_PHONE, {
  title: 'Permiso pa acceder a telefono',
  message: 'Nos da permiso para llamar?',
  buttonPositive: 'Dale on pare',
});
// Nota: En el emulador de IPhone no funciona pero en el real, sí
// ------- END Phone Call --------

// ------- Camera --------
// 1. instalar dependencia:
// https://github.com/react-native-community/react-native-image-picker
// ó
// https://docs.expo.io/versions/latest/sdk/imagepicker/
// 2. Link a la dependencia:
// ./node_modules/react-native/cli.js react-native link react-native-image-picker
// 3. Importar el ImagePicker
import ImagePicker from 'react-native-image-picker';
// 4. Establecer el options
const options = {
  title: 'Selecciona foto de perfil',
};
// 5. Abrir la camara
function nativeTakePhoto() {
  ImagePicker.showImagePicker(options, response => {
    console.log('Response = ', response);

    if (response.didCancel) {
      console.log('User cancelled image picker');
    } else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    } else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    } else {
      const source = {uri: response.uri};

      // You can also display the image using data:
      // const source = { uri: 'data:image/jpeg;base64,' + response.data };

      // Aquí podríamos subir la foto que tenemos en base64 a un servidor o bucket
      // como S3 y obtener una url que pasaríamos al webview vía injectJavaScript

      this.setState({
        avatarSource: source,
      });
    }
  });
}
// 6. Permisos:
// 6.1 Android:
// Añadir al AndroidManifest.xml:
// <uses-permission android:name="android.permission.CAMERA" />
// <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
// Activaremos el popup por si acaso. Aunque no tendría por qué estar
PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CAMERA, {
  title: 'Permiso pa acceder a camara',
  message: 'Nos da permiso para abrir la camara?',
  buttonPositive: 'Dale on pare',
});
// 6.2 iOS:
// Añadir la descripción para qué se usa en el info.plist NSCameraUsageDescription
// Si se va a permitir acceder a la librería, necesitaremos este NSPhotoLibraryUsageDescription
// Apple permisos: https://developer.apple.com/library/archive/documentation/General/Reference/InfoPlistKeyReference/Articles/CocoaKeys.html
// ------- END Camera --------

let myWebView;

const html = `
      <html>
      <head></head>
      <body>
        <div style="margin-top:400px" class="phone">+34 667 043 597</div>

        <div style="margin-top:400px" class="camera">Pulsa para subir foto</div>

        <script>
          function callPhone(number) {
            window.ReactNativeWebView.postMessage(JSON.stringify({
              type: 'phone',
              data: number
            }));
          };

          function takePhoto() {
            window.ReactNativeWebView.postMessage(JSON.stringify({
              type: 'camera'
            }));
          }

          let phoneElements = document.getElementsByClassName("phone");

          Array.from(phoneElements).forEach(function(element) {
            element.addEventListener('click', (event) => {
              var targetElement = event.target || event.srcElement;
              callPhone(targetElement.textContent);
            });
          });

          let cameraElements = document.getElementsByClassName("camera");

          Array.from(cameraElements).forEach(function(element) {
            element.addEventListener('click', (event) => {
              var targetElement = event.target || event.srcElement;
              takePhoto();
            });
          });
        </script>
      </body>
      </html>
    `;

const App: () => React$Node = () => {
  return (
    <WebView
      ref={ref => (myWebView = ref)}
      source={{html}}
      javaScriptEnabled={true}
      domStorageEnabled={true}
      startInLoadingState={true}
      geolocationEnabled={true}
      onMessage={event => {
        const action = event.nativeEvent.data
          ? JSON.parse(event.nativeEvent.data)
          : undefined;
        if (action && action.type === 'phone') {
          // Antes de pasar el action.data (el número de telefono), quitar espacios y validar que sólo tenga números y simbolo +
          // En caso de que no, lanzar un alert: alert('No es un número')
          // Sólo como prevención
          nativeCallPhone(action.data);
        } else if (action && action.type === 'camera') {
          nativeTakePhoto();
        } else {
          alert('Action not found');
        }
      }}
    />
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
